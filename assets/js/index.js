// slider

$(document).ready(function () {
  $(".sliderCardSection").slick({
    slidesToShow: 3,
    slidesToScroll: 3,
    dots: false,
    arrows: false,
    centerMode: true,
    centerPadding: "60px",
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          centerPadding: "40px",
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          centerPadding: "20px",
        },
      },
    ],
  });

  $(".prev").click(() => $(".sliderCardSection").slick("slickPrev"));
  $(".next").click(() => $(".sliderCardSection").slick("slickNext"));
});
// accordion

var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function () {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    }
  });
}

const form = document.getElementById("myForm");
form.addEventListener("submit", function (event) {
  event.preventDefault();
  const email = document.getElementById("email").value;
  const fullname = document.getElementById("fullname").value;
  const companyname = document.getElementById("companyname").value;
  const country = document.getElementById("country").value;
  const textarea = document.getElementById("textarea").value;

  clearErrorMessages();

  let hasError = false;

  if (!email || !isValidEmail(email)) {
    displayErrorMessage("email", "Please enter a valid email address.");
    hasError = true;
  }

  if (!fullname) {
    displayErrorMessage("fullname", "Full name is required.");
    hasError = true;
  }

  if (!companyname) {
    displayErrorMessage("companyname", "Company name is required.");
    hasError = true;
  }

  if (country === "none") {
    displayErrorMessage("country", "Please select a country.");
    hasError = true;
  }

  if (!textarea) {
    displayErrorMessage("textarea", "Message is required.");
    hasError = true;
  }

  if (hasError) {
    return;
  }

  form.submit();
  alert("submited");
});

function isValidEmail(email) {
  const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  return emailPattern.test(email);
}

function displayErrorMessage(fieldId, message) {
  const field = document.getElementById(fieldId);
  const errorMessage = document.createElement("span");
  errorMessage.classList.add("error-message");
  errorMessage.textContent = message;
  field.parentNode.appendChild(errorMessage);
}

function clearErrorMessages() {
  const errorMessages = document.querySelectorAll(".error-message");
  errorMessages.forEach(function (errorMessage) {
    errorMessage.parentNode.removeChild(errorMessage);
  });
}

// header

function myFunction(x) {
  x.classList.toggle("change");
}

let lastScrollTop = 0;
const header = document.querySelector(".header");

window.addEventListener("scroll", () => {
  let scrollTop = window.pageYOffset || document.documentElement.scrollTop;

  if (scrollTop > lastScrollTop) {
    // Scrolling down
    header.classList.add("hidden");
  } else {
    // Scrolling up
    header.classList.remove("hidden");
  }

  lastScrollTop = scrollTop <= 0 ? 0 : scrollTop;
});
// tabSection
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}
document.getElementById("defaultOpen").click();
